/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment.gcash;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Marlon Janssen Arao
 */
public final class Transaction {
    
    private static final String EMPTYSTRING = "";
    
    private int code;
    private String message = EMPTYSTRING;
    private long transid;
    private String trn = EMPTYSTRING;
    private String target = EMPTYSTRING;
    private double amount;
    private DateTime timestamp = new DateTime("1987-02-07T00:00:00.000+08:00");

    public int getCode() { return code; }
    public void setCode(int code) { this.code = code; }
    public String getMessage() { return message; }
    public void setMessage(String message) { 
        if (message != null) this.message = message; }
    public long getTransId() { return transid; }
    public void setTransId(long transId) { this.transid = transId; }
    public String getTrn() { return trn; }
    public void setTrn(String trn) { 
        if (trn != null) this.trn = trn; }
    public String getTarget() { return target; }
    public void setTarget(String target) { 
        if (target != null) this.target = target; }
    public double getAmount() { return amount; }
    public void setAmount(double amount) { this.amount = amount; }
    public DateTime getTimestamp() { return this.timestamp; }
    public void setTimestamp(DateTime timestamp) { 
        if (timestamp != null) this.timestamp = timestamp; }
    
    public void setTimestamp(String timestamp) { 
        if (timestamp != null) {
            DateTimeFormatter formatter = 
                DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            this.timestamp = formatter.parseDateTime(timestamp);
        }
    }
}
