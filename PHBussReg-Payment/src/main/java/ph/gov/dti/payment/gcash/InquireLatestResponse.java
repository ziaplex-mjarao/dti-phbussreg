/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment.gcash;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Marlon Janssen Arao
 */
public class InquireLatestResponse extends Response implements Serializable {
    
    private static final long serialVersionUID = 9081478620783329815L;
    
    private Transaction transaction = new Transaction();

    public Transaction getLatestTransaction() { return transaction; }
    public void setLatestTransaction(Transaction txn) { 
        if (txn != null) this.transaction = txn; }

    @Override
    public void fromJSONString(String response) {
        if (response != null && !response.isEmpty()) {
            try {
                JSONObject resp = new JSONObject(response);
                this.setCode(resp.getInt("code"));
                this.setMessage(resp.getString("message"));
                JSONObject ltstTxn = 
                        new JSONObject(resp.getJSONObject("latest_transaction"));
                transaction = new Transaction();
                transaction.setMessage(ltstTxn.getString("message"));
                transaction.setCode(ltstTxn.getInt("code"));
                transaction.setTransId(ltstTxn.getLong("transid"));
                transaction.setTrn(ltstTxn.getString("trn"));
                transaction.setTarget(ltstTxn.getString("target"));
                transaction.setAmount(ltstTxn.getDouble("amount"));
                transaction.setTimestamp(ltstTxn.getString("timestamp"));
            } catch (JSONException ex) { throw new RuntimeException(ex); }
        }
    }
}
