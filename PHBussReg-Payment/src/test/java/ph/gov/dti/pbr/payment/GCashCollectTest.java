/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.pbr.payment;

import org.junit.Before;
import org.junit.Test;
import ph.gov.dti.payment.GCashService;
import ph.gov.dti.payment.config.gcash.PaymentConfig;
import ph.gov.dti.payment.config.gcash.ServiceUrl;
import ph.gov.dti.payment.gcash.CollectResponse;
import ph.gov.dti.payment.gcash.InquireAllResponse;
import ph.gov.dti.payment.gcash.Request;
import ph.gov.dti.payment.gcash.Transaction;
/**
 *
 * @author Marlon Janssen Arao
 */
public class GCashCollectTest {

    static GCashService service = null;
    static Request rqst = null;

    public GCashCollectTest() { }

    @Before
    public void setUp() {
        service = new GCashService();
        rqst = new Request();

        service.setBaseUri(ServiceUrl.getBaseUri());
        service.setDbCoreApiKey("c44f778bf5ae5cb31f5ebf828046ae753c9ee581908c5d4f8d6ce4f8fbfc15ce");
        service.setUsername("dti_access");
        service.setPassword("dti_access");
    }
    

   // @Test
    public void testCollect() {
        System.out.println("Testing... collect method");
        String id = "dti_payment";
        String trn = "10120";
        double amount = 4000.75;

        rqst.setId(id);
        rqst.setPin(PaymentConfig.getPin());
        rqst.setTrn(trn);
        rqst.setAmount(amount);
        rqst.setTarget("09152002564");

        CollectResponse result = service.collect(rqst);
        System.out.print(">>>>> " + result.getCode());
        System.out.print(" | " + result.getMessage());
        System.out.println(" -- " + result.getTransId());
        System.out.print(">>>>>>> " + result.getLatestTransaction().getMessage());
        System.out.print(" | " + result.getLatestTransaction().getCode());
        System.out.println(" | " + result.getLatestTransaction().getTransId());
    }

    //@Test
    public void testInquireAll() {
        System.out.println("Testing... inquire all method");
        String trn = "10120";
        rqst.setTrn(trn);

        InquireAllResponse rspnse = service.inquireAll(rqst);
        for (Transaction txn : rspnse.getTransactions()) {
            System.out.print(">>>>> " + txn.getCode());
            System.out.print(" | " + txn.getMessage());
            System.out.println(" -- " + txn.getTransId());
        }
    }
}
