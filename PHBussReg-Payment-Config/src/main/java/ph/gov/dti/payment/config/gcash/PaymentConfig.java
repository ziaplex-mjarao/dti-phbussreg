/*
 * 
 */
package ph.gov.dti.payment.config.gcash;

/**
 *
 * @author Marlon Janssen Arao
 */
public final class PaymentConfig {
 
    public static String getPin() { return "1111"; }
    public static String getTarget() { return "09173124105"; }
    
    private PaymentConfig() {}
    public static PaymentConfig getInstance() { return ManagerHolder.INSTANCE; }
    private static class ManagerHolder {
        private final static PaymentConfig INSTANCE = 
                new PaymentConfig();
    }
}
