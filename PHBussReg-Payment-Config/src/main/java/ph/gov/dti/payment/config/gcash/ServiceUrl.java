/*
 * 
 */
package ph.gov.dti.payment.config.gcash;

/**
 *
 * @author Marlon Janssen Arao
 */
public final class ServiceUrl {
    
    public static String getBaseUri() {
        return "https://journey.globe.com.ph/.testing/rs/index.php/api/collect";
    }

    public static String getCollectPath() { return "sell"; }
    public static String getCountTransactionPath() { return "count_transaction"; }
    public static String getInquireAllPath() { return "inquire_all"; }
    public static String getInquireLatestPath() { return "inquire_latest"; }
    
    public static String getCollectUrl() { 
        return getBaseUri() +  "/" + getCollectPath(); 
    }
    
    public static String getCountTransactionUrl() { 
        return getBaseUri() + "/" + getCountTransactionPath(); 
    }
    
    public static String getInquireAllUrl() { 
        return getBaseUri() + "/" + getInquireAllPath(); 
    }
    
    public static String getInquireLatestUrl() {
        return getBaseUri() + "/" + getInquireLatestPath();
    }

    private ServiceUrl() {}
    public static ServiceUrl getInstance() { return ManagerHolder.INSTANCE; }
    private static class ManagerHolder {
        private final static ServiceUrl INSTANCE = new ServiceUrl();
    }
}
