/*
 * 
 */
package ph.gov.dti.payment.config.gcash;

/**
 *
 * @author Marlon Janssen Arao
 */
public final class AuthConfig {
    
    public static String getDbCoreApiKey() { 
        return "c44f778bf5ae5cb31f5ebf828046ae753c9ee581908c5d4f8d6ce4f8fbfc15ce"; }
    public static String getUsername() { return "dti_access"; }
    public static String getPassword() { return "dti_access"; }
    
    private AuthConfig() {}
    public static AuthConfig getInstance() { return ManagerHolder.INSTANCE; }
    private static class ManagerHolder {
        private final static AuthConfig INSTANCE = 
                new AuthConfig();
    }
}
